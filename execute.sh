#!/bin/bash

#Script for running the simulations.
#Instructions: Run this script with the following parameters:
#bash execute.sh number_of_repetitions selected_baseline input_file

# The input files can be found in the folder input/workload

# The baselines are as follows:

#0: NEMESIS
#1: NEMESIS NEW MIGRATIONS
#2: Baseilne 1 - https://doi.org/10.1016/j.jpdc.2019.09.015
#3: Baseline 2 (INTRA DCS MIGS) - https://doi.org/10.1016/j.jss.2021.110907
#4: Baseline 2 (INTER DCS MIGS) - https://doi.org/10.1016/j.jss.2021.110907
#5: Scorpious

for i in $(eval echo {1..$1});
do
   #Creating the output directory
   rm -rf "output_$i" 
   mkdir "output_$i"   
   #Generating the deploy file and moving it to the output folder
   python3 generate_deploy.py "$i" $2
   clear && clear && simulationmain homogeneousGrid5000Pstate.xml "output_$i/grid5000_deploy_$i.xml" $3  "--log=root.app:file:output_$i/logfile.log"
done
