{
  tinypkgs ? import (fetchTarball https://gitlab.com/migvasc/phd_nix_pkgs/-/archive/master/packages-repository-a90a172e5b6aa12ac643a124548e3e0ed0064b2a.tar.gz) {}
}:

tinypkgs.pkgs.mkShell rec {
  buildInputs = [
    tinypkgs.nemesis_mig_band
  ];
}