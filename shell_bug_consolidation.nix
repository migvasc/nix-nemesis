{
  tinypkgs ? import (fetchTarball https://gitlab.com/migvasc/phd_nix_pkgs/-/archive/master/packages-repository-11df272c6720db6e074b5827c698f793de02d6c1.tar.gz) {}
}:

tinypkgs.pkgs.mkShell rec {
  buildInputs = [
    tinypkgs.simulationmain_bug_consolidation
  ];
}