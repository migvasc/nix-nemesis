{
  tinypkgs ? import (fetchTarball https://gitlab.com/migvasc/phd_nix_pkgs/-/archive/master/packages-repository-48dd947157fab152a77b1b5eac39760fe8d171ef.tar.gz) {}
}:

tinypkgs.pkgs.mkShell rec {
  buildInputs = [
    tinypkgs.simulationmain_performance
  ];
}