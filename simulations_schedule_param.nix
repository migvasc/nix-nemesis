{
  tinypkgs ? import (fetchTarball https://gitlab.com/migvasc/phd_nix_pkgs/-/archive/master/packages-repository-f3e7e1ec1d2939bff33c8013eff6560dfcc09dd8.tar.gz) {}
}:

tinypkgs.pkgs.mkShell rec {
  buildInputs = [
    tinypkgs.simulations_schedule_param
    tinypkgs.migrations_no_congestion
  ];
}